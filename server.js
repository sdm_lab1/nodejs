const express=require('express')
const cors=require('cors')
const mysql=require('mysql2')


const connection=mysql.createConnection({
    // host:'localhost',
    host:'172.17.0.3',

    user:'root',
    password:'root',
    waitForConnections:true,
    // connectionLimit:10,
    database:'student'
})


const app=express()
app.use(cors('*'))
app.use(express.json())
connection.connect()

////////////////////////////////////////////

app.get('/',(req,res)=>{
    connection.query('select * from students_tb',(error,result)=>{
        if(error!=null)
            res.send(error)
        else
            res.send(result)    
    })
    
})

// insert into students_tb values(default,'Ashish','12345','DAC',2022,1234111,'1998-06-18');

app.post('/',(req,res)=>{
    const query=`insert into students_tb values(default,'${req.body.s_name}','${req.body.password}','${req.body.course}',${req.body.passing_year},${req.body.prn_no},'${req.body.dob}')`
    connection.query(query,(error,result)=>{
        if(error!=null)
            res.send(error)
        else
            res.send(result)  
    })
})
app.put('/:no',(req,res)=>{
    const query=`update students_tb set s_name='${req.body.s_name}',password='${req.body.password}',course='${req.body.course}',passing_year=${req.body.passing_year},prn_no=${req.body.prn_no},dob='${req.body.dob}' where student_id=${req.params.no}`;
    connection.query(query,(error,result)=>{
        if(error!=null)
        res.send(error)
        else
        res.send(result)  
    })
})

app.delete('/:no',(req,res)=>{
    const query=`delete from students_tb where dob='${req.params.no}'`
    connection.query(query,(error,result)=>{
        if(error!=null)
        res.send(error)
        else
        res.send(result)  
    })
})

////////////////////////////////////////
app.listen(3000,'0.0.0.0',()=>{
    console.log('server started at 3000' );
})